const productData = [

	{
		id: "ccc001",
		name: "Comandante C45 MKIII",
		description: "The Standard! The Hand Grinder to rule them all",
		price: 15999,
		onOffer: true
	},
	{
		id: "ccc002",
		name: "Wide Awake Coffee",
		description: "Ethiopia Bensa Woreda",
		price: 580,
		onOffer: true
	},
	{
		id: "ccc003",
		name: "Bottomless Portafilter",
		description: "Standard 58mm portafilter",
		price: 1500,
		onOffer: true
	}

]
